using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Numerics;
using System.Web.Mvc;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        //Gere um token válido em https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insira seu token JWT em INSERT_VALID_ACCESS_TOKEN

        static string INSERT_VALID_ACCESS_TOKEN = "";
        static string NAMESPACE = "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd";
        static string DADOSDIPLOMA = "DadosDiploma";
        static string DADOSREGISTRO = "DadosRegistro";
        static string INCLUDEXPATH = "includeXPathEnveloped";
        //static string DADOSDIPLOMANSF = "DadosDiplomaNSF";
        //static string DADOSREGISTRONSF = "DadosRegistroNSF";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Inicializar_Click_Dipl(object sender, EventArgs e)
        {
            try
            {
                //Preparando os dados para inicialização da assinatura
                string certificateData = this.TextBoxDadosCertificado.Text;
                string tipoAssinatura = this.DropDownRoleDoc.SelectedValue;
                this.DropDownRoleDoc.Enabled = false;
                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                string responseInitialize = Inicialize(certificateData, tipoAssinatura).Result;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //Desserialização
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                //Caso ocorra erro na desserialização
                if (responseInitializeObj.initializedDocuments == null)
                    throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //Pegando os dados da caixa de texto que é preenchida com retorno do Hub-FW
                this.TextBoxInicializarDoc.Text = responseInitialize;

                InputExtension inputExtension = new InputExtension();

                inputExtension.nonce = responseInitializeObj.nonce;
                inputExtension.algoritmoHash = "SHA256";
                inputExtension.formatoDadosEntrada = "BASE64";
                inputExtension.formatoDadosSaida = "BASE64";

                for (int i = 0; i < responseInitializeObj.signedAttributes.Count; ++i)
                {
                    var input = new ExtensionAssinaturas();
                    input.nonce = responseInitializeObj.signedAttributes[i].nonce;
                    input.hashes.Add(responseInitializeObj.signedAttributes[i].content);
                    inputExtension.assinaturas.Add(input);
                }

                this.TextBoxEntradaExtensao.Text = Serialize<InputExtension>(inputExtension);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }

        protected void Finalize_Click_Dipl(object sender, EventArgs e)
        {
            string certificateData = this.TextBoxDadosCertificado.Text;
            string tipoAssinatura = this.DropDownRoleDoc.SelectedValue;
            try
            {
                //Pegando os dados da caixa de texto que é preenchida com retorno do Hub-FW
                String responseInitialize = this.TextBoxInicializarDoc.Text;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //Desserialização do JSON retornado
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                OutputExtension outputExtension = Deserialize<OutputExtension>(this.TextBoxSaidaExtensao.Text);

                //Finalização da assinatura
                string responseFinalize = Finalize(certificateData, outputExtension, responseInitializeObj, tipoAssinatura).Result;
                //this.TextBoxFinalizar.Text = Finalize(outputExtension, responseInitializeObj).Result;
                this.TextBoxFinalizarDoc.Text = responseFinalize;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }

        public async static Task<string> Inicialize(string certificateData,string tipoAssinatura)
        {
            //Aqui montamos a requisição com os dados no formado Multipart
            var requestContent = new MultipartFormDataContent();

            //Define as mudanças na request para cada etapa da assinatura do Diploma
            string tagNodo = "nenhum";

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string nomeArquivo = null;
            switch (tipoAssinatura)
            {
                case "REITOR":
                    tagNodo = "diploma";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "EMISSORA":
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    tagNodo = "diploma";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "PESSOAFISICA":
                    tagNodo = "registro";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
                case "REGISTRADORA": //Caso IES Registradora XML Documentacao Academica
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "REGISTRADORADIPL": //Caso IES Registradora XML Diplomado
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
                case "HISTSECRETARIA": //Caso PF Rep. Secretaria XML Histórico Escolar
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("ADRT"), "profile");
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    break;
                case "HISTIESEMISSORA": //Caso PJ IES Emissora XML Histórico Escolar
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    break;
            }

            //Demais parâmetros
            requestContent.Add(new StringContent("123"), "nonce");
            requestContent.Add(new StringContent(certificateData), "certificate");
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
            requestContent.Add(new StringContent("ENVELOPED"), "signatureFormat");
            requestContent.Add(new StringContent("LINK"), "returnType");

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./" + nomeArquivo, FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            //É possível adicionar vários documentos aqui, para isto cada documento deve possuir um nonce      
            requestContent.Add(new StringContent("1"), "originalDocuments[0][nonce]");
            requestContent.Add(streamContentDocument, "originalDocuments[0][content]", nomeArquivo);

            if (tagNodo == "diploma")
            {
                requestContent.Add(new StringContent(DADOSDIPLOMA), "originalDocuments[0][specificNode][name]");
            }
            else if (tagNodo == "registro")
            {
                requestContent.Add(new StringContent(DADOSREGISTRO), "originalDocuments[0][specificNode][name]");
            }

            //Não enviará o Namespace no caso onde nodo assinado é o principal (raiz)
            if (tagNodo != "nenhum")
            {
                requestContent.Add(new StringContent(NAMESPACE), "originalDocuments[0][specificNode][namespace]");
            }

            HttpClient client = new HttpClient();

            //client.DefaultRequestHeaders.Add("fw_credencial", credencial);
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            var response = await client.PostAsync("https://diploma.hom.bry.com.br/api/xml-signature-service/v2/signatures/initialize", requestContent).ConfigureAwait(false);

            // O resultado do BRy Framework é um json
            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;

        }

        public async static Task<string> Finalize(string certificateData, OutputExtension outputExtension, ResponseInitialize responseInitializeObj, string tipoAssinatura)
        {
            var requestContent = new MultipartFormDataContent();
            string nomeArquivo = null;
            //Modifica o perfil de assinatura para o da etapa selecionada
            switch (tipoAssinatura)
            {
                case "REITOR":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    break;
                case "EMISSORA":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "EMISSORAENV":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "PESSOAFISICA":
                    nomeArquivo = "xml-diplomado.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile"); ;
                    break;
                case "REGISTRADORADIPL":
                    nomeArquivo = "xml-diplomado.xml";
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "HISTSECRETARIA":
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    requestContent.Add(new StringContent("ADRT"), "profile");
                    break;
                case "HISTIESEMISSORA":
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
            }

            //Parâmetros da Request de Finalize
            requestContent.Add(new StringContent(responseInitializeObj.nonce), "nonce");
            requestContent.Add(new StringContent(certificateData), "certificate");
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
            requestContent.Add(new StringContent("ENVELOPED"), "signatureFormat");
            requestContent.Add(new StringContent("SIGNATURE"), "operationType");
            requestContent.Add(new StringContent("LINK"), "returnType");

            //Essa parte pode ser feita em loop para adicionar mais Diplomas para assinatura
            requestContent.Add(new StringContent(outputExtension.assinaturas[0].hashes[0]), "finalizations[0][signatureValue]");
            requestContent.Add(new StringContent(outputExtension.assinaturas[0].nonce), "finalizations[0][nonce]");
            requestContent.Add(new StringContent(responseInitializeObj.initializedDocuments[0].content), "finalizations[0][initializedDocument]");

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./" + nomeArquivo, FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            requestContent.Add(streamContentDocument, "finalizations[0][content]", nomeArquivo);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            var response = await client.PostAsync("https://diploma.bry.com.br/api/xml-signature-service/v2/signatures/finalize ", requestContent).ConfigureAwait(false);

            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }


    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class ResponseInitialize
    {
        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public string nonce;

        [DataMember]
        public List<AssinaturasInicializadasIntern> initializedDocuments;

        [DataMember]
        public List<AssinaturasInicializadasIntern> signedAttributes;

        public ResponseInitialize()
        {
            this.initializedDocuments = new List<AssinaturasInicializadasIntern>();
            this.signedAttributes = new List<AssinaturasInicializadasIntern>();
        }
    }

    [DataContract]
    public class AssinaturasInicializadasIntern
    {

        [DataMember]
        public string content;

        [DataMember]
        public string nonce;

    }

    [DataContract]
    public class InputExtension
    {
        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string nonce;

        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public List<ExtensionAssinaturas> assinaturas;

        public InputExtension()
        {
            this.assinaturas = new List<ExtensionAssinaturas>();
        }
    }

    [DataContract]
    public class ExtensionAssinaturas
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public List<string> hashes;

        public ExtensionAssinaturas()
        {
            this.hashes = new List<string>();
        }
    }

    [DataContract]
    public class OutputExtension
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public List<ExtensionAssinaturas> assinaturas;

        public OutputExtension()
        {
            this.assinaturas = new List<ExtensionAssinaturas>();
        }
    }


}